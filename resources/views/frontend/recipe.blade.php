@extends('layouts.masterlayout')
@section('content')




<!-- /*start features -->
<div class="features txtCenter">
    <div class="container">
      
      <div class="row">
       
            
         <div class="col-sm-6 col-lg-6">
           <h2>{{$recipe->name}}</h2><br>
          
            <iframe width="450" height="350" src="{{$recipe->video}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        
     
         <!-- <img src="images/koshary.jpg" class="recipe"> -->
         {{-- <div id="rateYo"></div> --}}

       <!-- <button class="bttn upper ">  <a href="https://youtu.be/6YmjSZLjnoA">show video of recipe</a></button> -->
     </div>
         
       
       <div class="col-sm-6 col-lg-6">
         <ul class="nav nav-tabs">
             <li class="nav-item">
               <a class="nav-link active" href="#tab1" data-toggle="tab">About</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#tab2"data-toggle="tab">Ingrdients</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="#tab3"data-toggle="tab">Directions</a>
             </li>
             
           </ul>
           <div class="tab-content ">
             <div class="tab-pane active container" id="tab1">
               <h2>About</h2>
               <p>{{$recipe->About}}</p>
             </div>
           
             <div class="tab-pane container" id="tab2">
               <h2>Ingredients</h2>
               <ul>
                 {{$recipe->ingredients}}
               </ul>
             
             </div>
           
           
             <div class="tab-pane container" id="tab3">
               <h2>Direction</h2>
              <ul>
                  <li><p>
                    {{$recipe->direction}}
                 </p></li>
                  
                 </ul>
             </div>
           </div>
           
      </div>
     

    </div>


    </div>

   </div>

<!-- End features -->
<!-- start resturants -->

<div class="container">
<h1 >Popular Restaurants</h1>
<div class="row restaurant ">
       @foreach($restaurants as $restaurant)

             <div class="col-sm-6 col-lg-2">
       <img src="{{asset('img/restaurants/'.$restaurant->img)}}" class="img rounded-circle">
       
     </div>
     @endforeach
     

</div>
</div>
<!-- End restaurant -->
@endsection