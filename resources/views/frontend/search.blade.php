
@extends('layouts.masterlayout')
@section('content')

<!-- /*start features -->
 <div class="features txtCenter">
    <div class="container">
     
      <div class="row">
          @foreach ($recipes as $recipe )

        <div class="col-sm-6 col-lg-3">
          <img src="{{asset($recipe->image)}}" class="img">
          <h3>{{$recipe->name}}</h3>
          <a href={{route('recipe',$recipe->id)}}><button >Explore More</button></a><br><br>
        </div>
        @endforeach

    </div>

   </div>

<!-- End features -->
<!-- start resturants -->

<div class="container">

</div>
<!-- End restaurant -->

@endsection
