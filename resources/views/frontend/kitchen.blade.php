
@extends('layouts.masterlayout')
@section('content')
    


<!-- /*start features -->
 <div class="features txtCenter">
    <div class="container">
      <h1>{{$kitchen->name}}  kitchen</h1>
      <div class="row">
          @foreach ($recipes as $recipe )
                       
        <div class="col-sm-6 col-lg-3">
          <img src="{{asset($recipe->image)}}" class="img"><br>
          <h3>{{$recipe->name}}</h3>
          <a href={{route('recipe',$recipe->id)}}><button >Explore More</button></a><br><br>
        </div> 
        @endforeach
       
    </div>

   </div>

<!-- End features -->
<!-- start resturants -->

<div class="container">
<h1 >Popular Restaurants</h1>
<div class="row restaurant ">
    @foreach($restaurants as $restaurant)
       <div class="col-sm-6 col-lg-2">
   <img src="{{asset('img/restaurants/'.$restaurant->img)}}" class="img rounded-circle">
       </div>
@endforeach
     
</div>
</div>
<!-- End restaurant -->

@endsection