@extends('layouts.app');

@section("title","recipes")




@section("content")
<div class="container">



        <div class="row">

        <form method="post" action="{{url('/recipes')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="exampleFormControlSlect2" >name</label>
                <input class="form-control" name="title">
                <label for="exampleFormControlTextarea1" >About</label>
                <textarea class="form-control" name="about" rows="3"></textarea>
                <label for="exampleFormControlTextarea1" >ingredients</label>
                <textarea class="form-control" name="ingredients" rows="3"></textarea>
               <label for="exampleFormControlTextarea1" >direction</label>
                <textarea class="form-control" name="direction" rows="3"></textarea>
                <label for="exampleFormControlSlect2" >Url video</label>
                <input class="form-control" name="video">
                <label for="exampleFormControlSlect2">Kitchen</label>
                <label for="exampleFormControlTextarea1">image</label>
            <input type="file" name="image">

                <div class="form-select">
                    <select name="kitchen_id" class="form-select btn-secondary dropdown-toggle" aria-label="Default select example">
                      @foreach ($kitchens as $kitchen)

                      <option value="{{$kitchen->id}}">
                        {{$kitchen->name}}
                      </option>

                      @endforeach

                    </select>
                </div>
              </div>

              <input type="submit" class="btn btn-primary">
        </form>
        </div>
</div>


@endsection
