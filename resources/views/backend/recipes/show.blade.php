@extends('layouts.app');

@section("title","recipes")




@section("content")
<div class="container">





        <div class="row">

        <div class="card col-md-12" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title">{{$recipes->title}}</h5>
              <h6 class="card-subtitle mb-2 text-muted">{{$recipes->id}}</h6>
              <p class="card-text">{{$recipes->name}}</p>
              <p class="card-text">{{$recipes->About}}</p>
              <p class="card-text">{{$recipes->ingredients}}</p>
              <p class="card-text">{{$recipes->direction}}</p>
              <p class="card-text">{{$recipes->video}}</p>
              <p class="card-text">{{$recipes->kitchen_id}}</p>
              <a href="#" class="card-link">Card link</a>
              <a href="#" class="card-link">Another link</a>
            </div>
          </div>
        </div>
</div>

@endsection
