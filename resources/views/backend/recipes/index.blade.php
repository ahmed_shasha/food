@extends('layouts.app');

@section("title","recipes")




@section("content")
<div class="container">


     <div class="container-fluid">
      <a href="recipes/create/">Add New Recipes</a>
        <div class="row">

        @foreach ( $recipes as $recipes )

        <div class="card col-md-3" style="width: 30rem;">
            <div class="card-body">
              <h5 class="card-title"><a href="recipes/{{$recipes->id}}">{{$recipes->title}}</a></h5>
              <h6 class="card-subtitle mb-2 text-muted">{{$recipes->name}}</h6>
              {{-- <a href="recipes/{{$recipes->ingredients}}">ingredients</a> --}}
              {{-- <p class="card-text">{{$recipes->image}}</p> --}}
              <img src="{{asset(''.$recipes->image)}}" class=w-50 ><br>

              @auth
        <a class="btn btn-primary" href="recipes/{{$recipes->id}}/edit" class="card-link" style="color: white">Edit</a>

  <form method="post" action="recipes/{{$recipes->id}}">

      @csrf @method("delete")
      <button type='submit' class="btn btn-danger">Delete</button>
  </form>
  @endauth
            </div>
          </div>



     @endforeach

        </div>
     </div>
</div>


@endsection
