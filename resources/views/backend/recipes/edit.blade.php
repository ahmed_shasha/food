@extends('layouts.app');

@section("title","recipes")




@section("content")
<div class="container">

        <div class="row">

        <form method="post" action="{{ route('recipes.update',$recipes->id)}}" enctype="multipart/form-data">


            @csrf @method("put")



<div class="form-group">
                <label for="exampleFormControlSlect2" >name</label>
                <input class="form-control" name="title" value="{{$recipes->name}}">
                <label for="exampleFormControlTextarea1" >about</label>
                <input class="form-control" name="About" rows="3" value="{{$recipes->About}}">
                <label for="exampleFormControlTextarea1" >ingredients</label>
                <input class="form-control" name="ingredients" rows="3" value="{{$recipes->ingredients}}">
                <label for="exampleFormControlTextarea1" >direction</label>
                <input class="form-control" name="direction" rows="3" value="{{$recipes->direction}}">
                <label for="exampleFormControlSlect2" >Url video</label>
                <input class="form-control" name="video" value="{{$recipes->video}}">
                <label for="exampleFormControlTextarea1">image</label>
                <input type="file" name="images"><br>
                <label for="exampleFormControlSlect2">Kitchen</label>
                <div class="form-select">
                    <select name="kitchen_id" class="form-select btn-secondary dropdown-toggle" aria-label="Default select example">
                      @foreach ($kitchens as $kitchen)

                      <option value="{{$kitchen->id}}">
                        {{$kitchen->name}}
                      </option>

                      @endforeach

                    </select>
                </div>
              </div>


        <input type="submit" class="btn btn-primary">
        </form>
        </div>
</div>


@endsection
