<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FoodKing</title>
    <!--  CSS files -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet" >

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" >

   
    <link rel="stylesheet" href="{{asset('css/Egy.css')}}">
<link rel="stylesheet" href="{{asset('css/fontawesome.min.css')}}">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap">


</head>

<body>

    <!-- Start header  -->
    <header class="header txtColor cover navbar navbar-expand-lg navbar-light">
        {{-- <nav class="navbar navbar-expand-lg navbar-light   "> --}}
        <div class="overlay">
            <div class="container">
                {{-- <nav   class="navbar upper"> --}}
                    <span class="LogoFont ">food<span class="mainColor">king</span>
                    </span>
                  <div class="dropdown">
                    <button onclick="myFunction()" class="bttn txtColor upper dropbtn">
                        Menu <i class="fa fa-bars fa-lg"></i>
                    </button>
                    <div id="myDropdown" class="dropdown-content">
                  
                         <a href="{{url('/home')}}">Home</a>
                         @foreach( $kitchens as $kitchen)
                        <a href="{{route('kitchen',$kitchen->id)}}">{{$kitchen->name}}</a>
                        @endforeach 
                      
                    </div>
               
                  </div>
                  <script>
                    /* When the user clicks on the button,
                                                                                                                                                                                                                                                                    toggle between hiding and showing the dropdown content */
                    function myFunction() {
                        document.getElementById("myDropdown").classList.toggle("show");
                    }

                    // Close the dropdown if the user clicks outside of it
                    window.onclick = function(event) {
                        if (!event.target.matches('.dropbtn')) {
                            var dropdowns = document.getElementsByClassName("dropdown-content");
                            var i;
                            for (i = 0; i < dropdowns.length; i++) {
                                var openDropdown = dropdowns[i];
                                if (openDropdown.classList.contains('show')) {
                                    openDropdown.classList.remove('show');
                                }
                            }
                        }
                    }
                </script>

                 {{-- </nav> --}}
                 {{-- @yield('homesearch') --}}
                </header>
                @yield('content')

                <footer class="footer ">
                    <div class="container ">
                        <div class="content "><span class="copyright ">
                            © 2020 COPYRIGHT
                           <span class="LogoFont ">food<span class=" mainColor ">king</span></span>
                            </span>
                            <br>
                            <br>
                            <span class="design logoFont">DESIGNED BY team</span>
                        </div>
                        <div class="icons txtCenter ">
                            <i class="fa fa-facebook fa-lg "></i>
                            <i class="fa fa-twitter fa-lg "></i>
                            <i class="fa fa-google-plus fa-lg "></i>
                        </div>
                    </div>
                </footer>
                <!-- End footer -->

    <script src="{{asset('js/jquery-3.5.1.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>

       <script src="{{asset('js/popper.js')}}"></script>
      <script src="{{asset('js/bootstrap.min.js')}}"></script>

{{-- <script src="{{asset('js/Egy.js')}}"></script> --}}

</body>

</html>
