<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard','dashboardController@index');
// Route::get('/kitchen','KitchenController@index');
Route::resource('kitchens', 'KitchenController');
Route::resource('restaurants', 'RestaurantController');
Route::resource('recipes', 'RecipeController');
Route::get('home','homeController@index');
Route::get('kitchen/{id}','kitchen_controller@kitchen')->name('kitchen');
Route::get('recipe/{id}','kitchen_controller@recipe')->name('recipe');
Route::post('search','homecontroller@search')->name('search');

Auth::routes();

