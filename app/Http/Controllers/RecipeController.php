<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\recipe;

class Recipecontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $recipes = recipe::paginate(12);
        $recipes=recipe::all();
       return view ('backend.recipes.index',compact('recipes'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $kitchens=\App\kitchen::all();
        return view("backend.recipes.create",compact('kitchens'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title'=>'required',

                'image'=>'required',

                'about'=>'required',

                'ingredients'=>'required',

                'direction'=>'required',

            ]
              ) ;
             if($request->hasfile('image'))
             {
              $file=$request->file('image');
             $images=$this->uploadimages($file,"images");

             }
             else
             {
               $images="no-images.png";
             }
            $recipes=new recipe();
            $recipes->name=$request->title;
            $recipes->About=$request->about;
            $recipes->image=$images;
            $recipes->video=$request->video;
            $recipes->ingredients=$request->ingredients;
            $recipes->direction=$request->direction;
            $recipes->kitchen_id=$request->kitchen_id;

            // $post->user_id=Auth::user()->id;
            $recipes->save();

            // return redirect()->back();

            return redirect()->route('recipes.index')->withmsg('post saved sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipes=recipe::find($id);
     return view("backend.recipes.show",compact("recipes"));
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipes=recipe::find($id);
        $kitchens=\App\kitchen::all();
        return view("backend.recipes.edit",compact('recipes','kitchens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'title'=>'required | max:50 |min:5 ',
            ]
              ) ;
              if($request->hasfile('images'))
              {
               $file=$request->file('images');
              $images=$this->uploadimages($file,"images");

              }
              else
              {
                $images="no-images.png";
              }

            $recipes= recipe::find($id);
            $recipes->name=$request->title;
            $recipes->About=$request->About;
            $recipes->image=$images;
            $recipes->video=$request->video;
            $recipes->ingredients=$request->ingredients;
            $recipes->direction=$request->direction;
            $recipes->kitchen_id=$request->kitchen_id;
            $recipes->save();
            return redirect()->route('recipes.index')->withmsg('post saved sucessfully');
    }
    public function uploadimages($images,$dir)
{
  $file=$images;

  $images_name=$file->getClientOriginalName();//name.jpg
  $ext=$file->getClientOriginalExtension();//jpg
  $images='images/'.time().$images_name;

  $file->move($dir,$images);
  return $images;
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recipes=recipe::find($id);
    $recipes->delete();
    return redirect()->route('recipes.index')->withmsg('recipes deleted');

    }
}
