<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class searchcontroller extends Controller
{
    public function index()
    {
        $recipes=\App\recipe::all();
        return view("frontend.search",compact('recipes'));
    }
}
