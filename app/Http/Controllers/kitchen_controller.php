<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class kitchen_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     return view("frontend.kitchen");

    // }

    public function kitchen($id)

    {
        $kitchens=\App\kitchen::all();
        $kitchen=\App\kitchen::find($id);
        $recipes=\App\recipe::where("kitchen_id",$id)->get();
        $restaurants=\App\restaurant::where("kitchen_id",$id)->get();
        return view("frontend.kitchen",compact("recipes","restaurants","kitchen","kitchens"));
        
    }
    public function recipe($id)

    {
        // $kitchen=\App\recipe::find($id);
        // dd($kitchen);
        $kitchens=\App\kitchen::all();
        $recipe=\App\recipe::find($id);
        //  $restaurants=\App\restaurant::where("kitchen_id",$recipe->kitchen_id)->get();
        $restaurants=\App\kitchen::find($recipe->kitchen_id)->restaurant;
        // dd($restaurant);
        return view("frontend.recipe",compact("restaurants","recipe","kitchens"));
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
}